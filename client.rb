require 'socket'
require 'timeout'

PORT = 27015

puts 'Client started!'

s = TCPSocket.new 'localhost', PORT

puts 'Client connected!'

begin
  line = s.gets
  puts line
rescue
  #
end

client_message = gets.chomp

if client_message.empty?
  # close socket
  s.close
else
  begin
    # send message to server
    s.puts client_message
    puts

    begin
      Timeout::timeout(5) {
        # print server answer
        puts 'Server answers:'

        # Read lines from socket
        while line = s.gets
          # and print them
          puts line
        end
      }
    rescue
      # close socket
      s.close
    end

  rescue => e
    p e
    puts "Something went wrong :("

    # close socket
    s.close
  end
end

puts
puts '_' * 10
puts 'Client END!'