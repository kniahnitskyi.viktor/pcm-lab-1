require 'socket'

PORT = 27015

# Server bound to port 27015
server = TCPServer.new PORT

puts "Server started on port #{PORT}!"

client_count = 0

loop do
  # Wait for a client to connect
  client = server.accept
  puts 'Client connected!'
  puts
  client_count += 1
  client_id = client_count

  begin
    client.puts "What's your name?"

    # Print client message
    client_name = client.gets.chomp
    puts "Client #{client_id} has name:"
    puts client_name
    puts

    # Send message to client
    client.puts "Hello, #{client_name}, from server!"
    client.puts "Time is #{Time.now}"

    server_random_messages = [
      'Test message 1',
      'Test message 2',
      'Test message 3',
      'Test message 4',
      'Test message 5',
    ]

    types = %w[limited unlimited]
    type = types.sample

    if type == 'unlimited'
      begin
        while true
          server_message = server_random_messages.sample
          client.puts server_message
          sleep 1
        end
      rescue
        puts "Client #{client_name} with id = #{client_id} is not available..."
      end
    else
      3.times do
        server_message = server_random_messages.sample
        client.puts server_message
      end

      client.puts "Bye, #{client_name}!"

      # close client connection
      client.close
      puts "Close client #{client_id}"
      puts
    end
  rescue
    begin
      # try to close client connection
      client.close
    rescue
      # add log
    end
  end
end